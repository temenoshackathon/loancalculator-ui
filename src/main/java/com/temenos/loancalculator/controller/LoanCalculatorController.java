package com.temenos.loancalculator.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class LoanCalculatorController {

	@RequestMapping("/loancalculator")
	public String index() {
		return "index";
	}

	@RequestMapping(value = "/version", method = RequestMethod.GET)
	@ResponseBody
	public String getVersion() {
		return "v1.0";
	}
}
