package com.temenos.loancalculator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
@EnableAutoConfiguration
@ComponentScan({"com.temenos.loancalculator"})
public class LoancalculatorUiApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoancalculatorUiApplication.class, args);
		 System.out.println("Application starts..");
	}
	
}
