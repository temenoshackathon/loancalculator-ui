FROM java:8
EXPOSE 8080
ADD /target/loancalculator-ui-0.0.1-SNAPSHOT.jar loancalculator-ui.jar
ENTRYPOINT ["java","-jar","loancalculator-ui.jar"]